const express = require('express')
const dayjs = require('dayjs')
const app = express()

app.use(express.json());
app.use(express.text());
app.use(express.urlencoded({ extended: true }));

const port = process.env.PORT || 56201;

app.post('/square', (req, res)=> {
    const num = Number(req.body);
    if (isNaN(num)) {
        return res.status(400).send("Inserted parameter is not a number")
    }
    const result = num * num;
    res.status(200).send({
        number: num,
        square: result,
    });
});

app.post('/reverse', (req, res)=> {
    const insertedText = req.body;
    res.send(
        insertedText.split("").reverse().join("")
    );
});

const dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

function isLeapYear(year) {
    year = +year
    return (year % 100 === 0) ? ( year % 400 === 0) : (year % 4 === 0);
}

function isCorrectDate(day, month, year) {
    switch(month) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            return +day <= 31;
        case 4:
        case 6:
        case 9:
        case 11:
            return +day <= 30;
        case 2:
            return isLeapYear(year) ? +day <= 29 : +day <= 28;
        default:
            return false;
    }
}

app.get('/date/:year/:month/:day', (req, res)=> {
    const year = Number(req.params.year);
    const month = Number(req.params.month);
    const day = Number(req.params.day);

    if (!isCorrectDate(day, month, year)) {
        return res.status(400).send("Wrong parameters for date")
    } else {
        const insertedDate = new Date(req.params.year, --req.params.month, req.params.day);
        const currentDate = dayjs().startOf('day')
        res.send({
            'weekDay': dayNames[insertedDate.getDay()],
            'isLeapYear': isLeapYear(req.params.year),
            'difference': Math.abs(Number(currentDate.diff(insertedDate, 'day'))),
        });
    }
});

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
